// db.users.insertOne({
//     
//     "username": "Theoon",
//     "password": "angel123"
//     
//     
//     })
// 
// 
// db.users.insertOne({
// 
//     "username": "Angeeel",
//     "password": "angel1234"
// 
// })


//insert multiple documents at once
// db.users.insertMany(
// 
//     [
//         {
//             "username": "Sky07",
//             "password": "sky123"
//          },
//          
//          {
//             "username": "Thunder29",
//             "password": "thunder123"
//          }   
//     ]
// )
// 
// 


//create 3 new documents in a product collection with the following fields name,description,price




// db.products.insertMany(
//     [
//      {
//          "name":"Asus",
//          "description": "computer",
//          "price" : 55000
//      },
//      {
//          "name":"Dell",
//          "description": "laptop",
//          "price" : 65000
//      },
//      {
//      
//          "name":"Apple",
//          "description": "laptop",
//          "price" : 50000
//      }
//     
//     
//     ]
// )
// 






// db.collection.find()
// db.users.find()

// db.users.find({"username":"Theolique"})




// db.cars.insertMany(
//      
//      [
//      
//         {
//           "name": "Vios",
//           "brand": "Toyota",
//           "type":"sedan",
//           "price": "150000"
//          },
//          
//          {
//           "name": "TamarawFx",
//           "brand": "Toyota",
//           "type":"auv",
//           "price": "75000"
//          },
//          
//          {
//           "name": "City",
//           "brand": "Hondaa",
//           "type":"sedan",
//           "price": "160000"
//          }
//      
//      ]
//    
//      
//      )


// db.cars.find({"type":"sedan"})
// db.cars.find({"brand":"Toyota"})

//db.collection.findOne({"criteria":"value"}) --  db.cars.findOne({})

// db.cars.findOne({"type":"auv"})

// db.cars.findOne({"brand":"Hondaa"})


//db.collection.updateOne({"criteria": "value"}, {$set: {"field to be updated":"updated value"}})
// db.users.updateOne({"username": "peter199"}, {$set: {"username":"peter1999"}})

// db.users.updateOne({}, {$set: {"username":"updatedUsername"}}) -- updates the 1st item in the collection


//mongo will add the field that you add that does not yet exist:
//  db.users.updateOne({"username": "peter1999"}, {$set: {"isAdmin":"true"}})
 
 
// db.users.updateMany({}, {$set:{"isAdmin":true}})

//update all items that matches our criteria:
// db.cars.updateMany({"type":"sedan"}, {$set:{"price":1000000}})


//delete
// db.products.deleteOne({})


//delete first item that matches the criteria
// db.cars.deleteOne({})


//db.collection.deleteMany({"criteria":"value"}) = deletes all that matches the criteria
// db.users.deleteMany({"isAdmin":true})


// db.collection.deleteMany({})  ---> delete all documents in a collection


// db.products.deleteMany({}) -- deletes all 
// 
// db.cars.deleteMany({}) 
// 